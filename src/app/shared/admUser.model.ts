
export class AdmUser {
email: string = ''
​
email_verified: boolean = false;
​
last_name: string = ''
​
first_name: string = ''
​
locale: string = ''
​
full_name: string = ''
​
nickname: string = ''
​
picture: string = ''
​
sub: string = ''
​
updated_at: Date;

constructor(){
    this.updated_at = new Date();
}
}