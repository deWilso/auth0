import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AutenticationService {
  userOnChange :  BehaviorSubject <any>;
  constructor() { 
    this.userOnChange = new BehaviorSubject([]);
  }
}
