import { Component, OnInit, } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { DOCUMENT } from '@angular/common';
import { AutenticationService } from '../../services/autentication.service';
import { Subject} from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { AdmUser } from '../../shared/admUser.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
  ]
})
export class NavbarComponent implements OnInit {
  user: AdmUser;
  _unsubscribe: Subject <any>;
  constructor(public auth: AuthService, private AutenticationService: AutenticationService) {
    this._unsubscribe = new Subject();
    this.user = new AdmUser();
   }

  ngOnInit(): void {
    
    this.AutenticationService.userOnChange
    .pipe(takeUntil(this._unsubscribe))
    .subscribe(user => {
      this.user = user;      
    });
    
    this.getUser();
  
  }
  login(){
    this.auth.loginWithRedirect();      
  }

  logout(){
    this.auth.logout();
  }

  getUser(){
    this.auth.user$.subscribe(user => {
     
      if (user){
        this.user.email = user.email;
        this.user.email_verified = user.email_verified;
        this.user.first_name = user.given_name;
        this.user.last_name = user.family_name;
        this.user.full_name = user.name;
        this.user.nickname = user.nickname;
        this.user.picture = user.picture;
        this.user.sub = user.sub;
        this.user.updated_at = user.updated_at;
       
       
      }      
      this.AutenticationService.userOnChange.next(this.user);
      
    })
  }
}
