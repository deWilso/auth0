import { Component, OnInit } from '@angular/core';
import { AutenticationService } from '../../services/autentication.service';
import { AdmUser } from '../../shared/admUser.model';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-protegida',
  templateUrl: './protegida.component.html',
  styles: [
  ]
})
export class ProtegidaComponent implements OnInit {
  user: AdmUser;
  _unsubscribe: Subject <any>;
  constructor(private auth: AutenticationService) {
    this.user = new AdmUser();
    this._unsubscribe = new Subject();
   }
  
  ngOnInit(): void {
    this.auth.userOnChange
    .pipe(takeUntil(this._unsubscribe))
    .subscribe((user)=> {
      this.user = user;
    });
  }


}
